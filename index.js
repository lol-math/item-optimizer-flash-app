const { app, BrowserWindow } = require('electron');
app.commandLine.appendSwitch(
  'ppapi-flash-path',
  app.getPath('pepperFlashSystemPlugin'),
);
app.on('ready', () => {
  let win = new BrowserWindow({
    width: 1280,
    height: 720,
    autoHideMenuBar: true,
    useContentSize: true,
    resizable: false,
    titleBarStyle: 'customButtonsOnHover',
    title: 'Item Optimizer',
    icon: 'build/icon.ico',
    webPreferences: {
      plugins: true,
    },
  });
  win.setMenu(null);
  win.loadURL(`https://lolmath.net/LoL_Item_Optimizer.swf`);
  // Something else
});
